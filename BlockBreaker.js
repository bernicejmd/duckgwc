var rectlength = 100;
var rectwidth = 30;

fill(255, 0, 0); //upper first rectangle
rect(0,0,rectlength,rectwidth);
fill(51, 0, 255); //upper second rectangle
rect(100,0,rectlength,rectwidth);
fill(213, 255, 0); //upper third rectangle
rect(200,0,rectlength,rectwidth);
fill(43, 255, 0); //upper fourth rectangle
rect(300,0,rectlength,rectwidth);

fill(251, 255, 0); //lower first rectangle
rect(0,rectwidth ,rectlength,rectwidth);
fill(4, 255, 0); //lower first rectangle
rect(100,rectwidth ,rectlength,rectwidth);
fill(255, 0, 0); //lower first rectangle
rect(200,rectwidth ,rectlength,rectwidth);
fill(0, 26, 255); //lower first rectangle
rect(300,rectwidth ,rectlength,rectwidth);

fill(33, 26, 26);
ellipse(197,290,20,20); //ball
fill(222, 29, 64);
rect(150,300, rectlength,rectwidth/2); //main rectangle
